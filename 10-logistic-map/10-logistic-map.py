# Tasks:

# 1. Implement the logisitc map, which is given as follows:

#                 x_{n+1} = a*x_n*(1-x_n)

# 2. Visualize the bifurcation diagram for different values of the parameter a
# 3. Use neural network to predict the number of the logistic map 
# 4. Visualize the bifurcation map predicted by the neural network

import numpy as np
import matplotlib.pyplot as plt

class LogisticMap:
    def __init__(self, num_iterations=100, skip_iterations=90, seed = 0.5):
        self.num_iteration = num_iterations
        self.seed = seed
        self.skip_iterations = skip_iterations

    def logistic(self, r, x):
        # x_{n+1} = r*x_n*(1-x_n)
        return r * x * (1 - x)

    def run(self, fn=None):

        # ak nebola logisticka fn vlozena cez parameter, pouzijeme funkciu definovanu v self.logistic
        logistic_fn = self.logistic if fn is None else fn

        # x-os na grafe
        r_range = np.linspace(0, 4, 400)

        values = []
        for r in r_range:
            x = self.seed
            # opakovane aplikujeme logiscticku funkciu na predchadzajuci vysledok
            for i in range(self.num_iteration):
                # aby sme dostali cistejsi graf, preskocime prvych x iteracii
                if i > self.skip_iterations:
                    values.append((r, x))
                x = logistic_fn(r, x)
        return values

fig = plt.figure()
fig.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=0.5)


# inicializujeme logistic map
logisticMap = LogisticMap(
    num_iterations=300,
    skip_iterations=100,
    seed=0.5
)

#
# 1.,2. Visualize the bifurcation diagram for different values of the parameter a
# zobrazime logistic map s exaktne definovanou logistickou funkciou
print("Pocitam logistic map")
values = logisticMap.run()
ax1 = fig.add_subplot(311)
ax1.set_title("Exaktna logisticka fn.")
ax1.plot(*zip(*values), ls='', marker='.', markersize=1)

# Pripravime neuronovu siet na trenovanie 
NeuralNetwork = __import__('neural-network').NeuralNetwork
import datetime
nn = NeuralNetwork(size=(2,10,1))

# Pripravime trenovacie data pre trenovanie neuronovej siete
r_range = np.linspace(0, 4, 16)
training_data = []
for r in r_range:
    x = 0.5
    for i in range(20):
        result = LogisticMap.logistic(None, r, x)
        training_data.append((np.array([r, x]), np.array([result])))
        x = result

# Vytrenujeme neuronovu siet
start = datetime.datetime.now()
print("Trenujem neuronovu siet, velkost trenovacej mnoziny: " + str(len(training_data)))
inp, out = zip(*training_data)
errHistory = nn.train(
    inp,
    out,
    500, # epochs
    0.8  # learning rate
)[0]
print("Trenovanie trvalo:")
print(datetime.datetime.now() - start)


# zobrazime priebeh erroru ktory nastal pri trenovani neuronovej siete
shift = 100
errHistory = errHistory[shift:]
ax2 = fig.add_subplot(312)
ax2.set_title("Neuronova siet - error")
ax2.plot(range(shift, len(errHistory)+shift), errHistory)

# vytvorime funkciu ktora nahradi exaktnu logisticku funkciu. 
# Vo vnutri vyuziva na odhadovanie hodnot neuronovu siet
def logistic_neural_network(r, x):
    return nn.guess([r, x])[0][0]

# zobrazime log. fn. na zaklade neuronovej siete
print("Pocitam logistic map pomocou neuronovej siete")
values = logisticMap.run(fn=logistic_neural_network)
ax3 = fig.add_subplot(313)
ax3.set_title("Log. fn. na zaklade neuronovej siete")
ax3.plot(*zip(*values), ls='', marker='.', markersize=1)

plt.show()
