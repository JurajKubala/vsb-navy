import numpy as np
import random
import math

def sigmoid(x):
	return 1.0/(1.0 + np.exp(-x))

def sigmoid_der(x):
	return x*(1.0 - x)

class NeuralNetwork:
    def __init__(self, size):
        inputs, hiddens, outputs = size

        self.activation = sigmoid
        self.activation_der = sigmoid_der

        # vahy skrytej vrstvy
        self.weightsH = np.random.uniform(-1,1,(inputs, hiddens))
        self.biasH = np.random.uniform(-1, 1, (1, hiddens))

        # vahy vystupnej vrstvy
        self.weightsO = np.random.uniform(-1,1,(hiddens, outputs))
        self.biasO = np.random.uniform(-1, 1, (1, outputs))

    def train(self, inputs, outputs, epochs, learningRate):
        
        # pomocne premenne na logovanie historie
        errorHistory, weights_H_history, bias_H_history, weights_O_history, bias_O_history = list(), list(), list(), list(), list()

        n = -1
        print("Training: ", end=' ')    
        for i in range(epochs):
            # zobrazenie percentualneho priebehu treningu
            if i/(epochs/100) > n:
                n += 1
                print(100-n, end=' ')

            errors = list()
            # trenovanie NN pre jednotlive dvojice vstupu a ocakavaneho vystupu
            for idx in range(len(inputs)):
                err = self._train(inputs[idx], outputs[idx], learningRate)
                errors.append(err)

            # logovanie historie
            errors = np.ndarray.flatten(np.array(errors))
            errorHistory.append(sum(abs(errors)))
            weights_H_history.append(np.array(self.weightsH))
            weights_O_history.append(np.array(self.weightsO))
            bias_H_history.append(np.array(self.biasH))
            bias_O_history.append(np.array(self.biasO))

        print("")

        return [
            np.array(errorHistory),
            np.array(weights_H_history),
            np.array(bias_H_history),
            np.array(weights_O_history),
            np.array(bias_O_history)
        ]

    def _train(self, input, target, learningRate):

        # 
        # Feed forward
        # 
        
        net_H = input.dot(self.weightsH) + self.biasH
        out_H = self.activation(net_H)

        net_O = out_H.dot(self.weightsO) + self.biasO
        out_O = self.activation(net_O)
        
        # 
        # Back propagation
        # 

        error_O =  target - out_O
        delta_O = error_O * self.activation_der(out_O)
        error_H = delta_O.dot(self.weightsO.T)
        delta_H = error_H * self.activation_der(out_H)

        # upravit bias
        self.biasO += delta_O * learningRate
        self.biasH += delta_H * learningRate

        # upravit vahy vytupnej vrstvy
        out_H = out_H.reshape((1, -1)).T
        delta_O = delta_O.reshape((1, -1))
        self.weightsO += out_H.dot(delta_O) * learningRate

        # upravit vahy skrytej vrstvy
        delta_H = delta_H.reshape((1, -1))
        input = input.reshape((1, -1)).T
        self.weightsH += input.dot(delta_H) * learningRate

        return error_O

    def guess(self, input):
        input = np.array(input)
        net_H = input.dot(self.weightsH) + self.biasH
        out_H = self.activation(net_H)

        net_O = out_H.dot(self.weightsO) + self.biasO
        return self.activation(net_O)

