# Task: Implement two fern-like fractal models given by the following affine
# transformations:

#  |x'|   | a b c |   |x|   |j|
# w|y'| = | d e f | * |y| + |k|
#  |z'|   | g h i |   |z|   |l|


#  In the following tables, the values of the elements a-l are given. Each row represents the transformation.  The probability to use the transformation is p = 0.25. 

#  1st model:

#                      a      b     c     d     e     f     g      h    i     j     k     l 
# 1st transformation 0.00  0.00  0.01  0.00  0.26  0.00  0.00  0.00  0.05  0.00  0.00  0.00
# 2nd transformation 0.20 -0.26 -0.01  0.23  0.22 -0.07  0.07  0.00  0.24  0.00  0.80  0.00
# 3rd transformation -0.25  0.28  0.01  0.26  0.24 -0.07  0.07  0.00  0.24  0.00  0.22  0.00
# 4th transformation 0.85  0.04 -0.01 -0.04  0.85  0.09  0.00  0.08  0.84  0.00  0.80  0.00
 
 
#  2nd model:
 
#                       a     b     c     d    e     f     g     h     i   j     k   l
# 1st transformation  0.05  0.00  0.00  0.00 0.60  0.00  0.00  0.00  0.05 0.00 0.00 0.00
# 2nd transformation  0.45 -0.22  0.22  0.22 0.45  0.22 -0.22  0.22 -0.45 0.00 1.00 0.00
# 3rd transformation -0.45  0.22 -0.22  0.22 0.45  0.22  0.22 -0.22  0.45 0.00 1.25 0.00
# 4th transformation  0.49 -0.08  0.08  0.08 0.49  0.08  0.08 -0.08  0.49 0.00 2.00 0.00


# Visualize both IFSs.

import numpy as np
import matplotlib.pyplot as plt

# 1. Model
t1 = [
    [ 0.00,  0.00,  0.01,  0.00,  0.26,  0.00,  0.00,  0.00,  0.05,  0.00,  0.00,  0.00],
    [ 0.20, -0.26, -0.01,  0.23,  0.22, -0.07,  0.07,  0.00,  0.24,  0.00,  0.80,  0.00],
    [-0.25,  0.28,  0.01,  0.26,  0.24, -0.07,  0.07,  0.00,  0.24,  0.00,  0.22,  0.00],
    [ 0.85,  0.04, -0.01, -0.04,  0.85,  0.09,  0.00,  0.08,  0.84,  0.00,  0.80,  0.00]
]
p1 = [0.25, 0.25, 0.25, 0.25]

# 2. Model
t2 = [
    [ 0.05,  0.00,  0.00,  0.00, 0.60,  0.00,  0.00,  0.00,  0.05, 0.00, 0.00, 0.00],
    [ 0.45, -0.22,  0.22,  0.22, 0.45,  0.22, -0.22,  0.22, -0.45, 0.00, 1.00, 0.00],
    [-0.45,  0.22, -0.22,  0.22, 0.45,  0.22,  0.22, -0.22,  0.45, 0.00, 1.25, 0.00],
    [ 0.49, -0.08,  0.08,  0.08, 0.49,  0.08,  0.08, -0.08,  0.49, 0.00, 2.00, 0.00]
]
p2 = [0.25, 0.25, 0.25, 0.25]


#  |x'|   | a b c |   |x|   |j|
# w|y'| = | d e f | * |y| + |k|
#  |z'|   | g h i |   |z|   |l|
def transform(coords, transformation):
    # coords' = A*coords + B
    coords = np.array(coords)
    A = np.array(transformation[:-3]).reshape((3,3))
    B = np.array(transformation[-3:])
    return A.dot(coords) + B

def ifs(transformations, probabilities, num_points = 10000):
    points = [(0,0,0)]
    for _ in range(num_points):

        # nahodne zvolime transofrmaciu
        t_idx = np.random.choice(len(transformations), p=probabilities)
        transformation = transformations[t_idx]
        
        # transformujeme posledny vygenerovany bod
        new_point = transform(points[-1], transformation)

        # pridame bod do zoznamu bodov
        points.append(new_point)

    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')

    # Hide grid
    ax.grid(False)

    # Hide axes ticks
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_zticks([])

    ax.scatter(*zip(*points), s=0.2, edgecolor ='green')

# zobrazenie 1. modelu
ifs(t1, p1)

# zobrazenie 2. modelu
ifs(t2, p2)

plt.show()
