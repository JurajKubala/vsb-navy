import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation
from matplotlib import colors

neighbourhood = ((-1, -1), (-1, 0), (-1, 1),
                 (0, -1), (0, 1),
                 (1, -1), (1, 0), (1, 1))

cell_empty, cell_tree, cell_burning = 0, 1, 2

class ForestFireModel:

    def __init__(self, p_tree = 0.01, p_fire = 0.001, size = (100, 100)):
        self.p_tree = p_tree
        self.p_fire = p_fire
        self.size = size
        self.grid = np.random.random_integers(0, 1, size=self.size)

    def valid_boundaries(self, coordinates):
        rowIdx, colIdx = coordinates
        return True if rowIdx >= 0 and colIdx >= 0 and rowIdx < self.size[0] and colIdx < self.size[1] else False

    def is_any_neighbour_burning(self, coordinates):
        rowIdx, colIdx = coordinates
        return any([self.grid[rowIdx+row, colIdx+col] == cell_burning for row, col in neighbourhood if self.valid_boundaries( [rowIdx + row, colIdx + col] )])

    def iteration(self):
        new_grid = np.zeros(self.size)

        for rowIdx, row in enumerate(self.grid):
            for colIdx, cell in enumerate(row):
                
                # vyrastie novy strom
                if cell == cell_empty:
                    new_grid[rowIdx, colIdx] = cell_tree if np.random.random() < self.p_tree else cell_empty

                elif cell == cell_tree:
                    new_grid[rowIdx, colIdx] = cell_tree

                    # strom moze nahodne vzplanut
                    if np.random.random() < self.p_fire:
                        new_grid[rowIdx, colIdx] = cell_burning

                    # zapalime strom ak hori v blizkosti
                    if self.is_any_neighbour_burning([rowIdx, colIdx]):
                        new_grid[rowIdx, colIdx] = cell_burning
                   
        self.grid = new_grid
        return new_grid

    def run(self):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        colors_list = [(0,0,0), (0,0.5,0), (0.7,0,0)]
        cmap = colors.ListedColormap(colors_list)
        
        im = ax.imshow(self.grid, interpolation='antialiased', cmap=cmap, vmin=0, vmax=3)

        def animate(i):
            im.set_data(self.iteration())

        anim = animation.FuncAnimation(fig, animate, interval=60, save_count=300)
        # anim.save('12-forest-fire-model/12-forest-fire-model.gif', writer='imagemagick', fps=60)
        plt.show()


ForestFireModel().run()
