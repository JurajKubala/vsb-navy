# Task:

# Visualize the chaotic motion of the double pendulum. 

# Help:

# Use the presentation on the web pages of the subject:
# https://homel.vsb.cz/~ska206/navy/double_pendulum.pdf

import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt
import matplotlib.animation as animation

L1, L2 = 1, 1   # dlzka tyci
m1, m2 = 1, 1   # hmotnost 
g = 9.81        # gravitacne zrychlenie
animation_interval = 20
state = [
    2*np.pi/6, # uhol prveho ramena
    0,         # uhlove zrychlenie prveho ramena
    5*np.pi/8, # uhol druheho ramena
    0,         # uhlove zrychlenie druheho ramena
]

def get_derivative(state, y, L1, L2, m1, m2):
    # prepisane rovnice z prezentacie, 6str, https://homel.vsb.cz/~ska206/navy/double_pendulum.pdf
    theta1, theta1dot, theta2, theta2dot = state

    t12cos = np.cos(theta1 - theta2)
    t12sin = np.sin(theta1 - theta2)

    a = m2 * g * np.sin(theta2) * t12cos
    b = m2 * np.sin(theta1-theta2) * ( L1 * theta1dot**2 * t12cos + L2*theta2dot**2 )
    c = (m1 + m2) * g * np.sin(theta1)
    d = L1*( m1 + m2*t12sin**2 )
    theta1dot2 = (a - b - c) / d

    a = L1 * theta1dot**2 * t12sin - g*np.sin(theta2)
    b = g * np.sin(theta1) * t12cos
    c = m2 * L2 * theta2dot**2 * t12sin * t12cos
    d = L2*( m1 + m2*t12sin**2 )
    theta2dot2 = ((m1 + m2)*(a - b) + c) / d

    return theta1dot, theta1dot2, theta2dot, theta2dot2 

def get_coordinates():
    global state
    od = odeint(get_derivative, state, [0, animation_interval/1000], args=(L1, L2, m1, m2))[1]
    state = od

    # ziskame nove uhly ramien
    theta1 = od[0]
    theta2 = od[2]

    # vypocitame pozicie bodov na koncoch ramien
    x1 = L1 * np.sin(theta1)
    y1 = -L1 * np.cos(theta1)
    x2 = x1 + L2 * np.sin(theta2)
    y2 = y1 - L2 * np.cos(theta2)

    return x1, y1, x2, y2

# zobrazit graf
x1, y1, x2, y2 = get_coordinates()
fig, ax = plt.subplots(1,1)
ax_lim = L1 + L2 + 1
ax.set_xlim(-ax_lim, ax_lim)
ax.set_ylim(-ax_lim, ax_lim)
line1, = ax.plot([0, x1], [0, y1], '-bo')
line2, = ax.plot([x1, x2], [y1, y2], '-ro')

# vytvorime historiu pozicii tyci, aby sme mohli vykreslit stopu jednotlivych tyci
history_length = 50
history1 = list(np.zeros((history_length,2)))
history2 = list(np.zeros((history_length,2)))
line3, = ax.plot(*zip(*history1), color='#aaf', lw=0.5)
line4, = ax.plot(*zip(*history2), color='#faa', lw=0.5)

# kazdy animacny cyklus vypocitame pozicie tyci, a aktualizujeme graf
def animate(i):
    global history1, history2
    x1, y1, x2, y2 = get_coordinates()

    line1.set_data([0, x1], [0, y1])
    line2.set_data([x1, x2], [y1, y2])

    history1.append([x1, y1])
    history1 = history1[1:]
    line3.set_data(*zip(*history1))

    history2.append([x2, y2])
    history2 = history2[1:]
    line4.set_data(*zip(*history2))

# zobrazime graf
ani = animation.FuncAnimation(fig, animate, interval=animation_interval, save_count=300)
# ani.save('11-double-pendulum/11-double-pendulum.gif', writer='imagemagick', fps=60)
plt.show()