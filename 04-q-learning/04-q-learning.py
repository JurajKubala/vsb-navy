import random
import time
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
np.set_printoptions(suppress=True,linewidth=np.nan)

# mapa prostredia
world = ('*....'
         '.O..O'
         '.....'
         '...C.'
         '.....')
world_size = (5, 5)
actions =        ["up",   "right", "down", "left"]
actions_vector = [[-1,0], [0,1],   [1,0],  [0,-1]]

initial_state = world.find('*')
num_episodes = 30
learning_rate = 0.1

# hodnota sa postupne zmensuje z 1 az na 0 
# 1 = exploration, 0 = explotation (rozhodovanie sa na zaklade zistenych informacii)
epsilon = 1
epsilon_decrease_rate = 0.1

#
# Pomocne funkcie
#

def fromLinTo2D(pos):
    return (pos//world_size[0], pos%world_size[0])

def from2DToLin(pos):
    row, col = pos
    return row * world_size[0] + col

def isValidPosition(pos):
    return True if 0 <= pos[0] < world_size[0] and 0 <= pos[1] < world_size[1] else False

def getValidActions(pos):
    # najdeme vsetky mozne pohyby ktore mozu byt uskutocnene z danej pozicie
    return [i for i, vector in enumerate(actions_vector) if isValidPosition(pos + vector)]
    
def renderMap(state):
    # odstranime zakladnu poziciu z mapy
    worldMap = list(world.replace('*', '.'))
    worldMap[state] = '*'
    worldMap = np.reshape(worldMap, world_size)
    
    # spojime znaky v riadkoch a potom riadky
    worldMap = map(lambda row: "".join(row), worldMap)
    worldMap = "\n".join(worldMap)
    print( worldMap )

#    
# vytvorime maticu prostredia
#

r_matrix = np.full((len(world), len(world)), -1)
for idx, _ in enumerate(world):
    
    # konvertujeme poziciu z 1D do 2D priestoru
    current_pos = np.array(fromLinTo2D(idx))
    
    # najdeme vsetky mozne pohyby ktore mozu byt uskutocnene z danej pozicie
    possible_actions = getValidActions(current_pos)
    
    # pridame susedne policka do r_matrix matice 
    for _, action in enumerate(possible_actions):
        
        neighbour = current_pos + actions_vector[action]
        
        # zistime poziciu a znak v textovej reprezentacii mapy
        neighbourIdx = from2DToLin(neighbour)
        char = world[neighbourIdx]
        
        # na zaklade znaku na susdnom policku nastavime hodonotu do revard matite
        if char == '.' or char == '*':
            r_matrix[idx][neighbourIdx] = 0
        elif char == 'O':
            r_matrix[idx][neighbourIdx] = -100
        elif char == 'C':
            r_matrix[idx][neighbourIdx] = 100
            
#
# Pripravime Q tabulku. Inicializujeme ju na 0. Nevykonatelne akcie z daneho stavu nastavime na -inf aby nemohli byt zvolene pri volani argmax 
#

q_table = np.zeros((len(world), len(actions)))
# nastavime nevykonatelne akcie na -inf
for idx, _ in enumerate(world):
    current_pos = np.array(fromLinTo2D(idx))
    possible_actions = getValidActions(current_pos)
    invalid_actions = list(set([0,1,2,3]) - set(possible_actions))
    for i in invalid_actions:
        q_table[idx][i] = -np.inf


# pomocna premenna na ukladanie historie stavov
state_history = list()

#
# Q-learning algoritmus
#
for i in range(num_episodes):
    
    episode_state_history = list()
    
    # resetujeme stav na zaciatku kazdej epizody
    state = initial_state
    episode_state_history.append(state)
    
    while True:

        # konvertujeme poziciu z 1D do 2D priestoru
        current_pos = np.array(fromLinTo2D(state))
        
        if random.random() > epsilon:
            # exploitation
            # action = np.argmax(q_table[state]) vyberie vzdy prvu akciu ak je viac ako je akcia s max hodnotou
            q_row = q_table[state]
            action = np.random.choice(np.flatnonzero( np.isclose(q_row, q_row.max()) ))
        else:
            # exploration
            # zvolime nahodnu akciu
            # najdeme vsetky mozne pohyby ktore mozu byt uskutocnene z danej pozicie
            possible_actions = getValidActions(current_pos)
            action = random.choice(possible_actions)
            
        # aplikujeme akciu
        next_position = current_pos + actions_vector[action]
        next_state = from2DToLin(next_position)
        reward = r_matrix[state, next_state]
        
        # aktualizujeme Q tabulku
        # 𝑄(𝑠𝑡𝑎𝑡𝑒, 𝑎𝑐𝑡) = 𝑅(𝑠𝑡𝑎𝑡𝑒, 𝑎𝑐𝑡) + 𝛾 ∗ max([𝑛𝑒𝑥𝑡 𝑠𝑡𝑎𝑡𝑒, 𝑎𝑙𝑙 𝑎𝑐𝑡𝑖𝑜𝑛𝑠)]
        q_table[state, action] = reward + learning_rate * np.max(q_table[next_state])
        
        state = next_state
        episode_state_history.append(state)
    
        # ak sme nasli syr, alebo sme spadli do diery tak ukoncime epizodu
        if reward == 100 or reward == -100:
            break
    
    # zmensime epsilon aby sme sa z exploration fazy dostali postupne do exploitation fazy
    epsilon -= epsilon_decrease_rate
    
    state_history.append(episode_state_history)

#
# Vykreslenie grafov
#

def getMapMatrix():
    # odstranime zakladnu poziciu z mapy
    worldMap = world.replace('*', '.')
    
    worldMap = [0 if ch == '.' else ch for ch in worldMap]
    worldMap = [-100 if ch == 'O' else ch for ch in worldMap]
    worldMap = [100 if ch == 'C' else ch for ch in worldMap]
    
    return np.reshape(worldMap, world_size)

# priebeh poctu krokov za epizodu
fig1, ax = plt.subplots(1)
y = [len(states) for states in state_history]
ax.set_title("Pocet krokov za epizodu")
ax.plot(range(len(y)), y)

# zobrazime mapu prostredia
fig2, ax = plt.subplots(1)
pos = np.array(fromLinTo2D(initial_state))
mouseText = ax.text(pos[0], pos[1], "M", fontsize=18, ha='center', va='center')
ax.imshow(getMapMatrix(), cmap="PuOr")

# animacia pohybu mysi
episodeIdx = 0
stepIdx = 0
cheese_state = world.find('C')
def animate(i):
    global episodeIdx
    global stepIdx
    episode = state_history[episodeIdx]
    state = episode[stepIdx]
    found_cheese = episode[-1] == cheese_state
    
    # nastavime poziciu mysi
    pos = np.array(fromLinTo2D(state))
    mouseText.set_x(pos[1])
    mouseText.set_y(pos[0])
    mouseText.set_color('green' if found_cheese else 'red' )

    # nastavime nadpis nad grafom
    ax.set_title('Epizoda: {}, Krok: {}/{}, Nasla syr? {}'.format(
        episodeIdx+1, stepIdx+1, len(episode), 'Ano' if found_cheese else 'Nie'))

    # resetujeme pocitadla indexov ak sme sa dostali mimo 
    stepIdx += 1
    if stepIdx >= len(episode):
        stepIdx = 0
        episodeIdx += 1
        if episodeIdx >= len(state_history):
            episodeIdx = 0
    

state_history_size = len([item for sublist in state_history for item in sublist])
ani = animation.FuncAnimation(fig2, animate, interval=120, save_count=state_history_size)

# ulozime obrazky/animacie
# fig1.savefig('04-q-learning-1.png')
# ani.save('04-q-learning-2.gif', writer='imagemagick', fps=120)

plt.show()
