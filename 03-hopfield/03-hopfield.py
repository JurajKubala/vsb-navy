# Use the Hopfield network to save and recover patterns.

# Instructions:
# 1. Design several (at least three) patterns. Patterns are given by a 2D figure.
# 2. Use a Hopfield network to save these paterns.
# 3. Change your patterns and use the Hopfield network to recover it - use synchronous as well as asynchronous recover.

import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
import math
import random

class HopfieldNetwork:
    def __init__(self, rows, cols):
        self.rows = rows
        self.cols = cols
        self.weights = np.matrix([])
    
    def train(self, pattern):

        # vytvorenie vektoru z textoveho vzoru
        vector = self.createVectorFromPattern(pattern)

        # vytvorenie matice s vahami + vynulovanie diagonaly
        weightedMatrix = np.outer(vector, vector.T)
        np.fill_diagonal(weightedMatrix, 0)

        if self.weights.size == 0:
            # ak je matica so zapamatanymi vzormi prazdna, nastavime ju na vypocitanu maticu s vahami
            self.weights = weightedMatrix
        else:
            # pripocitame vypocitane vahy k uz existujucej matici
            self.weights += weightedMatrix

    def recoverSync(self, corruptedPatter):
        vector = self.createVectorFromPattern(corruptedPatter)
        s = np.dot(self.weights, vector)
        return self.createPatternFromVector(s)
 
    def recoverAsync(self, corruptedPatter):
        vector = self.createVectorFromPattern(corruptedPatter)
        columnIdxs = list(range(0, len(corruptedPatter)-1))
        random.shuffle(columnIdxs)
        # postupne v nahodnom poradi opravujeme poskodeny pattern
        for colIdx in columnIdxs:
            x = np.dot(self.weights.T[colIdx], vector)
            x = np.sign(x)

            # nastavenie obnovenej hodnoty
            np.put(vector, colIdx, x)

        return self.createPatternFromVector(vector)

    def createVectorFromPattern(self, pattern):
        # vytvorenie rows*cols velkej matice z textoveho vzoru
        patternMatrix = self.createMatrixFromPattern(pattern)

        # vytvorenie stlpcoveho vektoru
        return patternMatrix.transpose().flatten()

    def createPatternFromVector(self, vector):
        vector = list(map(np.sign, vector))
        patternMatrix = np.reshape(vector, (self.rows, self.cols))
        pattern = patternMatrix.transpose().flatten()
        return ['.' if q < 0 else 'X' for q in pattern]

    def createMatrixFromPattern(self, pattern):
        # parsovanie ./X do -1/1
        pattern = [-1 if ch == "." else 1 for ch in list(pattern)]
        
        # vytvorenie rows*cols velkej matice zo vzoru
        return np.reshape(pattern, (self.rows, self.cols))

    def drawPattern(self, pattern):
        pattern = [' ' if ch == "." else 'X' for ch in list(pattern)]
        patternMatrix = np.reshape(pattern, (self.rows, self.cols))

        result = "\n".join(
            map(" ".join, patternMatrix)
        )
        print(result)

# 
# 
# DEMO
# 
patternSize = 5
pattern1 = ('.XXX.'
            '.X.X.'
            '.XXX.'
            '.X.X.'
            '.XXX.')

pattern2 = ('.X...'
            '.X...'
            '.XXX.'
            '...X.'
            '...X.')

pattern3 = ('X...X'
            '.X.X.'
            '..X..'
            '.X.X.'
            'X...X')

pattern4 = ('..X..'
            '..X..'
            'XXXXX'
            '..X..'
            '..X..')

# 
# 1. Inicializacia Hopfieldovej siete
# 
hopfield = HopfieldNetwork(
    rows = patternSize,
    cols = patternSize
)
patterns = [pattern1, pattern2, pattern3, pattern4]

# 
# 2. Ucenie vzorov
# 
for i, pattern in enumerate(patterns):
    # print('\nTraining pattern\n')
    # hopfield.drawPattern(pattern)
    hopfield.train(pattern)

# 
# 3. Obnovenie poskodenych vzorov
# 

# fn na pridanie sumu do vzoru
def addNoise(pattern, noiseFactor):
    pattern = list(pattern)
    changesCnt = math.floor(len(pattern) * noiseFactor)
    changeIdxs = np.random.choice(list(range(0, len(pattern)-1)), size=int(changesCnt), replace=False)
    for idx in changeIdxs:
        pattern[idx] = '.' if pattern[idx] == 'X' else 'X'
    return "".join(pattern)

fig, axes = plt.subplots(ncols=4)
fig.set_size_inches(12, 3)
titles = ["Original", "With 10% noise", "Async recover", "Sync recover"]
for i, ax in enumerate(axes):
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    ax.set_title(titles[i])

def animate(i):
    # v kazdom animacnom cykle vyberieme nahodny pattern
    pattern = random.choice(patterns)
    axes[0].imshow(hopfield.createMatrixFromPattern(pattern), interpolation='nearest')

    # pridame sum
    pattern = addNoise(pattern, 0.1) # 10% noise
    axes[1].imshow(hopfield.createMatrixFromPattern(pattern), interpolation='nearest')

    # Async recover
    axes[2].imshow(hopfield.createMatrixFromPattern(hopfield.recoverAsync(pattern)), interpolation='nearest')
    
    # Sync recover
    axes[3].imshow(hopfield.createMatrixFromPattern(hopfield.recoverSync(pattern)), interpolation='nearest')

anim1 = animation.FuncAnimation(fig, animate, interval=2000, blit=False)
plt.show()