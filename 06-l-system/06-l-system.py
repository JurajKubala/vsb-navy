from turtle import Turtle, Screen

class LSystem:
    def __init__(self, axiom, rules, angle, title=""):
        self.axiom = axiom
        self.rules = rules 
        self.angle = angle
        self.title = title

    def get_instructions(self, iterations):
        result = self.axiom
        for _ in range(iterations):
            tmp = ""
            for char in result:
                # ak existuje prepisovacie pravidlo, nahradime nim znak
                tmp += self.rules[char] if char in self.rules else char
            result = tmp
        return result

    def draw(self, iterations = 2):

        # aplikujeme prepisovacie pravidla na zakladny axiom `iterations` krat
        instructions = self.get_instructions(iterations)

        # priprava na vykreslovanie
        turtle = Turtle()
        turtle.screen.title(self.title)
        turtle.speed(0)
        turtle.pensize(2)
        screen = Screen()
        screen.setup(640, 640)

        # vykreslime postupne jednotlive instrukcie
        for char in instructions:
            if char == 'F':
                turtle.forward(10)
            elif char == '+':
                turtle.right(self.angle)
            elif char == '-':
                turtle.left(self.angle)

        # nakoniec skryjeme korytnacku
        turtle.hideturtle()
        screen.exitonclick()
        
LSystem("F++F++F", {"F":"F-F++F-F"}, 60, "Koch's snowflake").draw(3)
# LSystem("F++F++F", {"F":"FF+F++F+F"}, 90, "Ice fractals").draw(3)
# LSystem("XF", {"X":"X+YF++YF-FX--FXFX-YF+", "Y": "-FX+YFYF++YF+FX--FX-Y"}, 60, "Gosper's curve").draw(3)
# LSystem("X", {"X":"-YF+XFX+FY-", "Y": "+XF-YFY-FX+"}, 90, "Gilbert's curve").draw(4)
