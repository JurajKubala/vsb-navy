# Task: 
# Use a fractal geometry to generate a simple landscape based on the following article:

# http://paulbourke.net/fractals/noise/

# Section: Fractal Landscapes

# Introduction

# Fractal landscapes are often generated using a technique called spatial subdivision. 
# For magical reasons this results in surfaces that are similar in appearance to the earth's terrain.

# The idea behind spatial subdivision is quite simple. Consider a square on the x-y plane,

#     (1) split the square up into a 2x2 grid
#     (2) vertically perturb each of the 5 new vertices by a random amount
#     (3) repeat this process for each new square decreasing the perturbation each iteration.

import random
import numpy as np
import matplotlib.pyplot as plt

# A roughness parameter. This is normally the factor by which the perturbations are reduced on each iteration. 
# A factor of 2 is the usual default, lower values result in a rougher terrain, higher values result in a smoother surface.
roughness = 4

# The initial perturbation amount. This set the overall height of the landscape.
max_perturbation = 10

# Sea level. This "flood" the terrain to a particular level simulating the water level.
sea_level = 0

# Number of iterations. This results in the density of the mesh that results from the iteration process.
num_iterations = 6

# inicializujeme mapu o velkosti 2x2
landscape = np.zeros((2,2))

for i in range(1, num_iterations + 1):
    # v kazdej iteraciu vytvorime novu mapu ktora je hustejsia. Rozdelime kazdy stvorec na styry nove
    new_landscape = np.zeros(( 2**i + 1, 2**i + 1 ))

    # prechadzame postupne kazdy stvorec na mape
    for row in range(landscape.shape[0] - 1):
        for col in range(landscape.shape[1] - 1):
            # a--ab--b
            # |   |  |
            # ac-mid-bd 
            # |   |  |
            # c--cd--d
            #
            # a,b,c,d su body z povodnej mapy
            # ab, ac, bd, cd, mid su nove body ktore pribudnu v novej mape
            a = landscape[row,col]
            b = landscape[row,col+1]
            c = landscape[row+1,col]
            d = landscape[row+1,col+1]

            ab = np.average([a, b]) 
            ac = np.average([a, c])
            bd = np.average([b, d])
            cd = np.average([c, d])
            mid = np.average([a,b,c,d])

            # nahodne pozmenime nove vygenerovane body
            new_points = [ab, ac, bd, cd, mid]
            for point_idx in range(len(new_points)):
                new_points[point_idx] += max_perturbation*(random.random() - 0.6)
                # nejdeme pod 'sea level'
                if new_points[point_idx] < sea_level:
                    new_points[point_idx] = sea_level 
            ab, ac, bd, cd, mid = new_points

            # nastavime nove body do novej mapy
            new_landscape[row*2, col*2] = a
            new_landscape[row*2, col*2+1] = ab
            new_landscape[row*2, col*2+2] = b
            
            new_landscape[row*2+1,col*2] = ac
            new_landscape[row*2+1,col*2+1] = mid
            new_landscape[row*2+1,col*2+2] = bd

            new_landscape[row*2+2,col*2] = c
            new_landscape[row*2+2,col*2+1] = cd
            new_landscape[row*2+2,col*2+2] = d

    # znizime perturbacny faktor
    max_perturbation /= roughness

    # nastavime novu mapu ako aktualnu
    landscape = new_landscape


fig = plt.figure()
ax = fig.gca(projection='3d')

# Hide grid
ax.grid(False)

# Hide axes ticks
ax.set_xticks([])
ax.set_yticks([])
ax.set_zticks([])

# Vykreslime teren
X, Y = np.mgrid[-1:1:1j*landscape.shape[0],-1:1:1j*landscape.shape[1]]
surf = ax.plot_surface(X, Y, landscape, cmap='gist_earth')

plt.show()
