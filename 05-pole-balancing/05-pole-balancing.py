# Consider a Pole-balancing problem wit a one pole. 

# Task:
# 1. Use a Q-learning as a supervisor for a neural network solving this problem 
# 2. For implementation use gym package, where the pole-balancing problem is prepared
# 3. Visualize the final solution 

import gym
import math
import random
import time
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

class PoleBalancingProblem:

    def __init__(self, 
        num_episodes=600, 
        bucket_size=(1, 1, 6, 3),
        learning_rate_initial = 1,
        learning_rate_decay = 0.01,
        epsilon_initial = 1,
        epsilon_decay = 0.01,
        show_simulation = True
    ):
        self.env = gym.make('CartPole-v0')
        self.num_episodes = num_episodes
        self.learning_rate = learning_rate_initial
        self.learning_rate_decay = learning_rate_decay
        self.epsilon = epsilon_initial
        self.epsilon_decay = epsilon_decay
        self.show_simulation = show_simulation

        self.bucket_size = bucket_size
        self.q_table = np.zeros(self.bucket_size + (self.env.action_space.n,))
        
        obs_space = self.env.observation_space
        self.bucket_boundaries = list([0,0,0,0])
        self.bucket_boundaries[0] = [obs_space.low[0], obs_space.high[0]]   # 𝑥 … position of a cart
        self.bucket_boundaries[1] = [-0.5, 0.5]                             # 𝑣 … velocity of a cart
        self.bucket_boundaries[2] = [obs_space.low[2], obs_space.high[2]]   # 𝜃 … angle of a pole
        self.bucket_boundaries[3] = [-math.radians(50), math.radians(50)]   # 𝑤 … angular velocity of a pole

    def get_next_action(self, state):
        if random.random() > self.epsilon: # exploitation
            # podobne ako np.argmax, avsak pri zhode vyberie nahodny index, nie prvy ako to robi argmax
            q_row = self.q_table[state]
            action = np.random.choice(np.flatnonzero( np.isclose(q_row, q_row.max()) ))
        else: # exploration
            action = self.env.action_space.sample()
        return action

    def update_q_table(self, old_state, action, new_state, reward):
        self.q_table[old_state][action] += self.learning_rate * (reward + np.max(self.q_table[new_state]) - self.q_table[old_state][action])

    def update_epsilon_lr(self):
        self.epsilon =self.clamp(0.1, 1, self.epsilon * (1 - self.epsilon_decay) )
        self.learning_rate = self.clamp(0.1, 1, self.learning_rate * (1 - self.learning_rate_decay) )

    def clamp(self, a, b, value):
        return max(a, min(b, value))

    def discretize_observation(self, observation):
        buckets = []
        for obs_idx, obs in enumerate(observation):
            b_low, b_high = self.bucket_boundaries[obs_idx]
            max_bucket_idx = self.bucket_size[obs_idx] - 1
            
            # ak je pozorovana hodnota mensia ako najnizsia pripustna hodnota, pozorovanie zaradime do prvej priehradky
            if obs <= b_low: 
                buckets.append(0)

            # ak je pozorovana hodnota vacsia ako najvacsia pripustna hodnota, pozorovanie zaradime do poslednej priehradky
            elif obs >= b_high: 
                buckets.append(self.bucket_size[obs_idx] - 1)
                
            # ak je pozorovanie v medziach pripustnych hodnot, vypocitame priehradku do ktorej ho zaradime
            else:
                # cielom nasledujucich operacii je projekcia intervalu [b_low, b_high] do [0, bucket_size]
                # posunieme honotu pozorovania tak, ako by interval pripustnych hodnot zacinal v 0
                obs = obs - b_low
                # sirka intervalu pripustnych hodnot
                b_width = b_high - b_low
                # vypocitame scale_ration ktore pouzijeme pri projekcii hodnot do intervalu [0, bucket_size]
                scale_ratio = self.bucket_size[obs_idx] / b_width
                # projekcia pozorovania do intervalu [0, bucket_size]
                scaled_observation = obs * scale_ratio
                # index prehriadky ziskame operaciou math.floor
                buckets.append(math.floor(scaled_observation))

        return tuple(buckets)

    #
    # Q-learning algoritmus
    #
    def run(self):

        steps_history = list()
        learning_rate_history = list()
        epsilon_history = list()

        for e_idx in range(self.num_episodes):
            # print("Epizoda: " + str(e_idx), "Epsilon: " + str(self.epsilon))

            # na zaciatku epizody resetujeme prostredie
            observation = self.env.reset()

            # vypocitame aktualny stav na zaklade dat z modelu
            current_state = self.discretize_observation(observation)

            done = False
            i = 0
            while done == False:
                # vykreslime aktualny stav v poslednych 10 epizodach
                if self.show_simulation and e_idx >= self.num_episodes-1:
                    self.env.render()

                # zvolime dalsiu akciu
                action = self.get_next_action(current_state)

                # aplikujeme akciu na aktualny stav
                observation, reward, done, info = self.env.step(action)

                # vypocitame novy stav na zaklade dat z modelu
                new_state = self.discretize_observation(observation)

                # aktualizujeme Q tabulku 
                self.update_q_table(current_state, action, new_state, reward)

                # aktualizujeme aktualny stav
                current_state = new_state
                i += 1
 
            steps_history.append(i)
            learning_rate_history.append(self.learning_rate)
            epsilon_history.append(self.epsilon)

            self.update_epsilon_lr()

        return steps_history, learning_rate_history, epsilon_history

#
# Spustenie simulacie
#
steps, lr, epsilon =  PoleBalancingProblem(
    learning_rate_initial=0.6,
    show_simulation=True
).run()

#
# Grafy
#
fig, axs = plt.subplots(3,sharex=True)

axs[0].set_title("Pocet krokov za epizodu")
axs[0].plot(range(len(steps)), steps)

axs[1].set_title("Learning rate")
axs[1].plot(range(len(lr)), lr)

axs[2].set_title("Exploration rate")
axs[2].plot(range(len(epsilon)), epsilon)

plt.show()