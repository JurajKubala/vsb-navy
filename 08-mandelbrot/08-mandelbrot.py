# Task:

# Implement the Mandelbrot Set with zoom

# Mandelbrot set is defined as the set of complex numbers c, where the sequence
# z_0, z_1, ... is limited, i.e. it satisfies the condition:

# there exists a real number m such that for all n |z_n| <= m (|.| is the absolute value)

# The sequence is generated according to the following equation:

# z_0  = 0, z_{n+1} = z_n^2 + c
# and m = 2

import numpy as np
import matplotlib.pyplot as plt

class Mandelbrot:

    def __init__(self, threshold=30, size=(300, 300), x_limit=(-1, 1), y_limit=(-1, 1)):
        self.threshold = threshold
        self.size = size
        self.x_limit = x_limit
        self.y_limit = y_limit

    def get_iterations(self, c):
        m = 2
        z_n = complex(0, 0)
        iteration = 0

        for iteration in range(1, self.threshold):
            z_n = z_n*z_n + c
            if abs(z_n) > m:
                return iteration
        return np.NaN

    def mandelbrot(self):
        
        xAxis = np.linspace(*self.x_limit, num=self.size[0])
        yAxis = np.linspace(*self.y_limit, num=self.size[1])

        matrix = np.zeros((len(xAxis), len(yAxis)))

        for x_idx, x in enumerate(xAxis):
            for y_idx, y in enumerate(yAxis):
                point = complex(x, y)
                matrix[y_idx, x_idx] = self.get_iterations(point)
        return matrix
        
    def zoom_factory(self, ax, imshow, base_scale = 1.4):
        def zoom(event):

            if event.button == 'down':
                # deal with zoom in
                scale_factor = 1 / base_scale
                self.threshold += 1
            elif event.button == 'up':
                # deal with zoom out
                scale_factor = base_scale
                self.threshold -= 1
            else:
                # deal with something that should never happen
                scale_factor = 1

            ax_xlim = ax.get_xlim()
            ax_ylim = ax.get_ylim()

            # relativna pozicia mysi voci grafu
            mouse_x_rel = (event.xdata - ax_xlim[0])/(ax_xlim[1] - ax_xlim[0])
            mouse_y_rel = (event.ydata - ax_ylim[1])/(ax_ylim[0] - ax_ylim[1])

            # sirka/vyska mandelbrotovej mnoziny
            width_x = self.x_limit[1] - self.x_limit[0]
            width_y = self.y_limit[1] - self.y_limit[0]

            # pozicia mysi voci mandelbrotovej mnozine
            mouse_x = mouse_x_rel*width_x + self.x_limit[0]
            mouse_y = mouse_y_rel*width_y + self.y_limit[0]

            # aplikujeme zoom
            width_x_scaled = width_x*scale_factor
            width_y_scaled = width_y*scale_factor

            # nastavime nove limity pre pocitanie mandelbrotovej mnoziny
            self.x_limit = [mouse_x - width_x_scaled*mouse_x_rel, mouse_x - mouse_x_rel*width_x_scaled + width_x_scaled]
            self.y_limit = [mouse_y - width_y_scaled*mouse_y_rel, mouse_y - mouse_y_rel*width_y_scaled + width_y_scaled]

            # vykreslime graf
            matrix = self.mandelbrot()
            imshow.set_clim(vmin=np.nanmin(matrix))
            imshow.set_clim(vmax=np.nanmax(matrix))
            imshow.set_data(matrix) 
            ax.figure.canvas.draw()

        fig = ax.get_figure() # get the figure of interest
        fig.canvas.mpl_connect('scroll_event', zoom)

        return zoom

    def run(self):

        fig = plt.figure()

        ax = fig.add_subplot(111, autoscale_on=True)
        
        # Hide axes ticks
        ax.set_xticks([])
        ax.set_yticks([])

        matrix = self.mandelbrot()
        imshow = ax.imshow(matrix, interpolation="nearest", cmap="plasma", vmin=np.nanmin(matrix), vmax=np.nanmax(matrix))
        figZoom = self.zoom_factory(ax, imshow)
        plt.show()

mandelbrot = Mandelbrot(
    threshold=20,
    size=(200, 200),
    x_limit=(-2,1),
    y_limit=(-1.5,1.5)
)
mandelbrot.run()