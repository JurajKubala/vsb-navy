# Consider a line given by the following equation:
 
#  y = 4x - 5
 
# Task: 

# Generate the set of 100 points and use a perceptron to find out whether the 
# points lie above or below the line.

# Instructions:

# Visualize the line and the points (use different colors for points lying above 
# or below the line)  

import numpy as np
import matplotlib.pyplot as plt

def lineFormula(x):
    return 4*x - 5

def lineFormulaInv(y):
    return (5 + y)/4

def pointPosition(x, y):
    if y > lineFormula(x):
        return 1     # nad priamkou
    elif y < lineFormula(x):
        return -1    # pod priamkou
    else:
        return 0     # na priamke

class Perceptron:
    def __init__(self, learningRate):
        # na zacaiatku su vahy a bias nahodne vygenerovane v rozsahu <-1, 1>, [Wx, Wy, b]
        self.weights = np.random.uniform(-1,1,3) 
        self.activationFn = np.sign
        self.learningRate = learningRate

    # vrati hodnotu pre dany bod (x, y) na zaklade aktualnych vah
    def evaluate(self, x, y):
        w = self.weights
        return self.activationFn(w[0]*x + w[1]*y + w[2])

    def train(self, x, y, z):
        # vypocita chybu pre dany bod (x, y) pri aktualnom nastaveni vah
        guess = self.evaluate(x, y)
        error = z - guess
        
        # upravi vahy na zaklade vypocitanej chyby
        self.weights[0] = self.weights[0] + error*x*self.learningRate
        self.weights[1] = self.weights[1] + error*y*self.learningRate
        self.weights[2] = self.weights[2] + error*self.learningRate


perceptron = Perceptron(learningRate = 0.1)

# Velkost grafu a minimalna/maximalna hodnota x a y
graphSize = 30
maxXY = graphSize / 2
minXY = -maxXY 

#
# 1. Training
#
# generovanie dat na trenovanie percetronu
trainingDatasetSize = 100000
trainingPoints = list(zip(np.random.uniform(minXY, maxXY, trainingDatasetSize),
                          np.random.uniform(minXY, maxXY, trainingDatasetSize)))
trainingData = [(x, y, pointPosition(x, y)) for (x,y) in trainingPoints]

# trenovanie perceptronu
for idx, point in enumerate(trainingData):
    perceptron.train(*point)

#
# 2. Testing
#

# nastavenie grafu
fig, ax = plt.subplots()
plt.xlim(minXY, maxXY)
plt.ylim(minXY, maxXY)

# generovanie testovacich bodov
testDatasetSize = 100
testPoints = list(zip(np.random.uniform(minXY, maxXY, testDatasetSize),
                      np.random.uniform(minXY, maxXY, testDatasetSize)))

# hodnotenie bodov pomocou perceptronu, vysledok je pole (x, y, guess)
result = [(x, y, perceptron.evaluate(x, y)) for (x, y) in testPoints]

# zobrazenie priamky v grafe
ax.plot([lineFormulaInv(minXY), lineFormulaInv(maxXY)], [minXY, maxXY], linestyle='solid')

# zobrazenie bodov v grafe. Ak je bod nad ciarou tak je cerveny, inak je modry
for idx, (x, y, guess) in enumerate(result):
    color = "red" if guess >= 0 else "blue"
    ax.scatter(x, y, color=color)

plt.show()