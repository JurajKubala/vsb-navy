# Nekonvenční algoritmy a výpočty (NAVY)

- [x] Perceptron: point on the line, support
- [x] Simple neural network: XOR problem
- [x] Hopfield network
- [x] Q-learning and the game Find the cheese
- [x] Pole-balancing problem
- [x] L-systems
- [ ] IFS
- [ ] TEA - Mandelbrot set or Julia's set
- [ ] Generation of 2D country using fractal geometry
- [ ] Theory of chaos: Logistic map, chaotic numbers and their prediction
- [ ] Chaotic motion - double pendulum
- [ ] Cellular automata - game of life, forest fire algorithm