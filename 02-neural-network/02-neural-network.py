# https://homel.vsb.cz/~ska206/navy/Neural%20Networks.pdf
# Task: Use a neural network to solve the XOR problem.
# Table:
#   | x | y | XOR|
#   --------------
#   | 0 | 0 |  0 |
#   --------------
#   | 0 | 1 |  1 |
#   --------------
#   | 1 | 0 |  1 |
#   --------------
#   | 1 | 1 |  0 |
# Help:
# This problem cannot be solved using only one perceptron

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib.gridspec as gridspec
import random
import math

def sigmoid(x):
	return 1.0/(1.0 + np.exp(-x))

def sigmoid_der(x):
	return x*(1.0 - x)

def relu(x):
    return x * (x > 0)

def relu_der(x):
    return 1. * (x > 0)

class NeuralNetwork:
    def __init__(self, size, fn = "sigmoid"):
        inputs, hiddens, outputs = size

        if fn == "relu":
            self.activation = relu
            self.activation_der = relu_der
        else:
            self.activation = sigmoid
            self.activation_der = sigmoid_der

        # vahy skrytej vrstvy
        self.weightsH = np.random.uniform(-1,1,(inputs, hiddens))
        self.biasH = np.random.uniform(-1, 1, (1, hiddens))

        # vahy vystupnej vrstvy
        self.weightsO = np.random.uniform(-1,1,(hiddens, outputs))
        self.biasO = np.random.uniform(-1, 1, (1, outputs))

    def train(self, inputs, outputs, epochs, learningRate):
        
        # pomocne premenne na logovanie historie
        errorHistory, weights_H_history, bias_H_history, weights_O_history, bias_O_history = list(), list(), list(), list(), list()

        n = -1
        print("Training: ", end=' ')    
        for i in range(epochs):
            # zobrazenie percentualneho priebehu treningu
            if i/(epochs/100) > n:
                n += 1
                print(100-n, end=' ')

            errors = list()
            # trenovanie NN pre jednotlive dvojice vstupu a ocakavaneho vystupu
            for idx in range(len(inputs)):
                err = self._train(inputs[idx], outputs[idx], learningRate)
                errors.append(err)

            # logovanie historie
            errors = np.ndarray.flatten(np.array(errors))
            errorHistory.append(sum(abs(errors)))
            weights_H_history.append(np.array(self.weightsH))
            weights_O_history.append(np.array(self.weightsO))
            bias_H_history.append(np.array(self.biasH))
            bias_O_history.append(np.array(self.biasO))

        print("")

        return [
            np.array(errorHistory),
            np.array(weights_H_history),
            np.array(bias_H_history),
            np.array(weights_O_history),
            np.array(bias_O_history)
        ]

    def _train(self, input, target, learningRate):

        # 
        # Feed forward
        # 
        
        net_H = input.dot(self.weightsH) + self.biasH
        out_H = self.activation(net_H)

        net_O = out_H.dot(self.weightsO) + self.biasO
        out_O = self.activation(net_O)
        
        # 
        # Back propagation
        # 

        error_O =  target - out_O
        delta_O = error_O * self.activation_der(out_O)
        error_H = delta_O.dot(self.weightsO.T)
        delta_H = error_H * self.activation_der(out_H)

        # upravit bias
        self.biasO += delta_O * learningRate
        self.biasH += delta_H * learningRate

        # upravit vahy vytupnej vrstvy
        out_H = out_H.reshape((1, -1)).T
        delta_O = delta_O.reshape((1, -1))
        self.weightsO += out_H.dot(delta_O) * learningRate

        # upravit vahy skrytej vrstvy
        delta_H = delta_H.reshape((1, -1))
        input = input.reshape((1, -1)).T
        self.weightsH += input.dot(delta_H) * learningRate

        return error_O

    def guess(self, input):
        input = np.array(input)
        net_H = input.dot(self.weightsH) + self.biasH
        out_H = self.activation(net_H)

        net_O = out_H.dot(self.weightsO) + self.biasO
        return self.activation(net_O)

#
#
# Vyber z dvoch ukazok
#
selectedDemo = "xor"
# selectedDemo = "wine-quality"

if selectedDemo == "xor":
    
    #
    # XOR Demo
    #

    inp=np.array([ [0,0], [0,1], [1,0], [1,1] ])
    out=np.array([ [0],   [1],   [1],   [0]   ])
    epochs = 10000
    learningRate = 0.5

    network = NeuralNetwork(size=(len(inp[0]), 3, len(out[0])))

    # trening siete
    result = network.train(inp, out, epochs, learningRate)

    # vypis do konzoly
    for i in range(len(inp)):
        target = out[i][0]
        guess = network.guess(inp[i])[0][0]
        print(("Ocakavany vysledok {} vs odhadnuty {}, rozdiel: {}".format(target, guess, target - guess)))

elif selectedDemo == "wine-quality":

    #
    # Wine quality demo
    # 

    # nacitanie dat
    inp, out = list(), list()
    import csv
    with open('winequality-white.csv', 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter=';')
        next(reader) # skip first line
        for row in reader:
            row = list(map(float, row))
            inp.append(row[:-1])
            out.append(row[-1:])

    # zmensit sample size kvoli casovej uspore
    sample = random.sample(list(zip(inp, out)), k=30) 
    inp, out = list(zip(*sample))
    inp, out = np.array(inp), np.array(out)

    def normalize(x):
        return (x-min(x))/(max(x)-min(x)) 

    # normalizovanie input/output dat
    inpT = inp.T
    for i in range(len(inpT)):
        inpT[i] = normalize(inpT[i])
    inp = inpT.T

    out = normalize(out)

    epochs = 5000
    learningRate = 0.5
    network = NeuralNetwork(size=(len(inp[0]), 7, len(out[0])))
    result = network.train(inp, out, epochs, learningRate)
    for i in range(10):
        target = out[i][0]
        guess = network.guess(inp[i])[0][0]
        print(("Ocakavany vysledok {} vs odhadnuty {}, rozdiel: {}".format(target, guess, target - guess)))

    ######## Koniec Wine quality demo

else:
    print("Unknown demo selected")
    exit(1)

#
# Grafy a vizualizacia
#

errHistory = result[0]
weights_H_history = result[1]
bias_H_history = result[2]
weights_O_history = result[3]
bias_O_history = result[4]

fig = plt.figure(constrained_layout=True)
gs = gridspec.GridSpec(ncols=2, nrows=3, figure=fig)

# Error graf - inicializacia
ax1 = fig.add_subplot(gs[0, :])
ax1.set_title("Learning rate: {:1.2f}".format(learningRate))
ax1.set_ylabel("Error")
ax1.set_xlim(0, epochs)
ax1.set_ylim(0, errHistory.max())

# Vahy skrytej vrstvy - inicializacia
weights_H_cnt = len(weights_H_history[0].flatten())
ax2 = fig.add_subplot(gs[1, 0])
ax2.set_title("Hidden weights")
ax2.set_ylim(weights_H_history.min() - 0.2, weights_H_history.max() + 0.2)
weights_H_bar  = ax2.bar(list(range(weights_H_cnt)), np.zeros(weights_H_cnt))

# Bias skrytej vrstvy - inicializacia
bias_H_cnt = len(bias_H_history[0].flatten())
ax3 = fig.add_subplot(gs[1, 1])
ax3.set_title("Hidden bias")
ax3.set_ylim(bias_H_history.min() - 0.2, bias_H_history.max() + 0.2)
bias_H_bar  = ax3.bar(list(range(bias_H_cnt)), np.zeros(bias_H_cnt))

# Vahy vystupnej vrstvy - inicializacia
weights_O_cnt = len(weights_O_history[0].flatten())
ax4 = fig.add_subplot(gs[2, 0])
ax4.set_title("Output weights")
ax4.set_ylim(weights_O_history.min() - 0.2, weights_O_history.max() + 0.2)
weights_O_bar  = ax4.bar(list(range(weights_O_cnt)), np.zeros(weights_O_cnt))

# Bias vystupnej vrstvy - inicializacia
bias_O_cnt = len(bias_O_history[0].flatten())
ax5 = fig.add_subplot(gs[2, 1])
ax5.set_title("Output bias")
ax5.set_ylim(bias_O_history.min() - 0.2, bias_O_history.max() + 0.2)
bias_O_bar  = ax5.bar(list(range(bias_O_cnt)), np.zeros(bias_O_cnt))

line, = ax1.plot([], [])
    
# parameter ktory redukuje pocet animacnych cyklov, cim vacsi tym je animacia rychlejsia
frames = 50
def animate(i):
    i = math.floor(i*(epochs/frames))

    line.set_data(list(range(i)), errHistory[:i])

    for j, bar in enumerate(weights_H_bar):
        bar.set_height(weights_H_history[i].flatten()[j])

    for j, bar in enumerate(bias_H_bar):
        bar.set_height(bias_H_history[i].flatten()[j])

    for j, bar in enumerate(weights_O_bar):
        bar.set_height(weights_O_history[i].flatten()[j])

    for j, bar in enumerate(bias_O_bar):
        bar.set_height(bias_O_history[i].flatten()[j])

anim1 = animation.FuncAnimation(fig, animate, frames=frames, interval=20, blit=False, repeat_delay=1000)

# anim1.save('myAnimation.gif', writer='imagemagick', fps=30)
plt.show()
